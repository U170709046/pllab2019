import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


public class abc {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        int lines = 0;
        while (reader.readLine() != null) lines++;
        reader.close();


        Scanner scanner = new Scanner(new File("/home/sumeyra/java_workspace/lab/labA/test.txt"));
        int[] values = new int [lines];
        int c = 0;
        while(scanner.hasNextInt())
        {
            values[c++] = scanner.nextInt();
        }

        TreeMap tree = new TreeMap<>();

        for (int i = 0; i < values.length; i++) {
            tree.put(i, values[i]);
        }

        LinkedHashMap lhm = new LinkedHashMap();

        for (int i = 0; i < values.length; i++) {
            lhm.put(i, values[i]);
        }

        long startTime = System.nanoTime();
        for (int i = 0; i < 5 ; i++) {
            System.out.println(tree.ceilingEntry(i));
        }
        long estimatedTime = System.nanoTime() - startTime;
        System.out.println("TreeMap: " +estimatedTime + " ns");


        long startTime2 = System.nanoTime();
        for (int i = 0; i < 5 ; i++) {
            System.out.println(lhm.get(i));
        }
        long estimatedTime2 = System.nanoTime() - startTime2;
        System.out.println("LinkedHashMap: " + estimatedTime2 + " ns");

    }

}



